//: Playground - noun: a place where people can play
//Code by Jay from thechalakas.com

import UIKit

var str = "Hello, playground"

print(str)

//Topic #1 - Functions

/*
 
 func funcname(Parameters) -> returntype {
 Statement1
 Statement2
 ---
 Statement N
 return parameters
 }
 
 */

//function with parameters

func helloworld1(str: String) -> Int
{
    print("The string that was sent as parameter is " + str)
    
    //just returning 5 just like that
    return 5;
}

//call the function and get the return value.
var tempReturnedValue = 0
tempReturnedValue = helloworld1(str:str)

print("The value returned from the function is " + String(tempReturnedValue))

//function without paramters

func helloworld2()
{
    print("This is a function without parameters and no return value")
}

//function without parameters but return value.

func helloworld3() -> String
{
    let tempstr1 = "this string was returned"
    return tempstr1
}

//Lets do some class tuff

class catone
{
    var color:String
    var age:Int
    
    //initializers
    init()
    {
        color = "red"
        age = 5
    }
    
    func showstuff()
    {
        print("color is " + color + " and the age is " + String(age))
    }
}

let catonetemp1 = catone()
catonetemp1.showstuff()



//Lets look at Collections

//array

let sqrts: [Double] = [1, 1.414, 1.732, 2.236, 2.646, 3.317]

for index in sqrts
{
    print( "Value of index is \(index)")
}

//Dictionary

var someDict:[Int:String] = [1:"One", 2:"Two", 3:"Three"]
var oldVal = someDict.updateValue("New value of one", forKey: 1)
var someVar = someDict[1]

print( "Old value of key = 1 is \(String(describing: oldVal))" )
print( "Value of key = 1 is \(String(describing: someVar))" )
print( "Value of key = 2 is \(someDict[2])" )
print( "Value of key = 3 is \(someDict[3])" )


//Generics and using them in Functions

func exchange<T>(a: inout T, b: inout T) {
    let temp = a
    a = b
    b = temp
}
var numb1 = 100
var numb2 = 200

print("Before Swapping Int values are: \(numb1) and \(numb2)")
exchange(a: &numb1, b: &numb2)
print("After Swapping Int values are: \(numb1) and \(numb2)")

var str1 = "Generics"
var str2 = "Functions"

print("Before Swapping String values are: \(str1) and \(str2)")
exchange(a: &str1, b: &str2)
print("After Swapping String values are: \(str1) and \(str2)")

//this is the protocol
protocol classa
{
    var marks: Int { get set }
    var result: Bool { get }
    
    func attendance() -> String
    func markssecured() -> String
}

//the class that uses the protocol
class classb : classa
{
    var marks: Int = 0
    
    var result: Bool
    
    func attendance() -> String
    {
        return("return something")
    }
    
    func markssecured() -> String
    {
        return("return something")
    }
    
    init()
    {
        //some stuff
        marks = 20
        result = false
    }
    
    
}

//Extensions
extension String
{
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString)
    }
}

let newString = "the old bike".replace(target: "old", withString: "new")
print(newString) // "the new bike"


//Subscripts

class daysofaweek {
    private var days = ["Sunday", "Monday", "Tuesday", "Wednesday",
                        "Thursday", "Friday", "saturday"]
    subscript(index: Int) -> String {
        get {
            return days[index]
        }
        set(newValue) {
            self.days[index] = newValue
        }
    }
}
var p = daysofaweek()

print(p[0])
print(p[1])
print(p[2])
print(p[3])
















